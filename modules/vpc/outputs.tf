output "vpc_id" {
  description = "The ID of the VPC"
  value = concat(aws_vpc.this.*.id, [""])[0]
}

output "vpc_arn" {
  description = "The ARN of the VPC"
  value = concat(aws_vpc.this.*.arn, [""])[0]
}

output "vpc_cidr_block" {
  description = "The CIDR block of the VPC"
  value = concat(aws_vpc.this.*.cidr_block, [""])[0]
}

output "private_subnets" {
  description = "List of IDs of private subnets"
  value = aws_subnet.private.*.id
}

output "private_subnet_arns" {
  description = "List of ARNs of private subnets"
  value = aws_subnet.private.*.arn
}

output "private_subnets_cidr_blocks" {
  description = "List of cidr_blocks of private subnets"
  value = aws_subnet.private.*.cidr_block
}

output "public_subnets" {
  description = "List of IDs of public subnets"
  value = aws_subnet.public.*.id
}

output "public_subnet_arns" {
  description = "List of ARNs of public subnets"
  value = aws_subnet.public.*.arn
}

output "public_subnets_cidr_blocks" {
  description = "List of cidr_blocks of public subnets"
  value = aws_subnet.public.*.cidr_block
}

output "database_subnets" {
  description = "List of IDs of database subnets"
  value = aws_subnet.database.*.id
}

output "database_subnet_arns" {
  description = "List of ARNs of database subnets"
  value = aws_subnet.database.*.arn
}

output "database_subnets_cidr_blocks" {
  description = "List of cidr_blocks of database subnets"
  value = aws_subnet.database.*.cidr_block
}

output "database_subnet_group" {
  description = "ID of database subnet group"
  value = concat(aws_db_subnet_group.database.*.id, [""])[0]
}

output "elasticache_subnet_group" {
  description = "ID of elasticache subnet group"
  value = concat(aws_elasticache_subnet_group.elasticache.*.id, [""])[0]
}

output "lambda_subnets" {
  description = "List of IDs of lambda subnets"
  value = aws_subnet.lambda.*.id
}

output "public_route_table_ids" {
  description = "List of IDs of public route tables"
  value = aws_route_table.public.*.id
}

output "private_route_table_ids" {
  description = "List of IDs of private route tables"
  value = aws_route_table.private.*.id
}

output "database_route_table_ids" {
  description = "List of IDs of database route tables"
  value = length(aws_route_table.database.*.id) > 0 ? aws_route_table.database.*.id : aws_route_table.private.*.id
}

output "nat_ids" {
  description = "List of allocation ID of Elastic IPs created for AWS NAT Gateway"
  value = aws_eip.nat.*.id
}

output "nat_public_ips" {
  description = "List of public Elastic IPs created for AWS NAT Gateway"
  value = aws_eip.nat.*.public_ip
}

output "igw_id" {
  description = "The ID of the Internet Gateway"
  value = concat(aws_internet_gateway.this.*.id, [""])[0]
}

# Static values (arguments)
output "azs" {
  description = "A list of availability zones specified as argument to this module"
  value = var.azs
}

output "name" {
  description = "The name of the VPC specified as argument to this module"
  value = var.name
}
