variable "name" {
  description = "Name to be used on all the resources as identifier"
  type = string
}

variable "environment" {
  description ="Environment - Workspace"
  type = string
}

variable "cidr" {}

variable "instance_tenancy" {
  description = "A tenancy option for instances launched into the VPC"
  type = string
  default = "default"
}

variable "public_subnet_suffix" {
  description = "Suffix to append to public subnets name"
  type = string
  default = "public"
}

variable "private_subnet_suffix" {
  description = "Suffix to append to private subnets name"
  type = string
  default = "private"
}

variable "database_subnet_suffix" {
  description = "Suffix to append to database subnets name"
  type = string
  default = "db"
}

variable "lambda_subnet_suffix" {
  description = "Suffix to append to lambda subnets name"
  type = string
  default = "lambda"
}

variable "public_subnets" {
  description = "A list of public subnets inside the VPC"
  type = list(string)
  default = []
}

variable "private_subnets" {
  description = "A list of private subnets inside the VPC"
  type = list(string)
  default = []
}

variable "database_subnets" {
  description = "A list of database subnets"
  type = list(string)
  default = []
}

variable "lambda_subnets" {
  description = "A list of lambda subnets"
  type = list(string)
  default = []
}

variable "create_database_subnet_route_table" {
  description = "Controls if separate route table for database should be created"
  type = bool
  default = false
}

variable "create_database_internet_gateway_route" {
  description = "Controls if an internet gateway route for public database access should be created"
  type = bool
  default = false
}

variable "create_database_nat_gateway_route" {
  description = "Controls if a nat gateway route should be created to give internet access to the database subnets"
  type = bool
  default = false
}

variable "create_database_subnet_group" {
  description = "Controls if database subnet group should be created"
  type = bool
  default = true
}

variable "create_elasticache_subnet_group" {
  description = "Controls if elasticache subnet group should be created"
  type = bool
  default = true
}

variable "azs" {
  description = "A list of availability zones in the region"
  type = list(string)
  default = []
}

variable "enable_dns_hostnames" {
  description = "Should be true to enable DNS hostnames in the VPC"
  type = bool
  default = true
}

variable "enable_dns_support" {
  description = "Should be true to enable DNS support in the VPC"
  type = bool
  default = true
}

variable "enable_nat_gateway" {
  description = "Should be true if you want to provision NAT Gateways for each of your private networks"
  type = bool
  default = false
}

variable "single_nat_gateway" {
  description = "Should be true if you want to provision a single shared NAT Gateway across all of your private networks"
  type = bool
  default = false
}

variable "one_nat_gateway_per_az" {
  description = "Should be true if you want only one NAT Gateway per availability zone. Requires `var.azs` to be set, and the number of `public_subnets` created to be greater than or equal to the number of availability zones specified in `var.azs`."
  type = bool
  default = false
}

variable "reuse_nat_ips" {
  description = "Should be true if you don't want EIPs to be created for your NAT Gateways and will instead pass them in via the 'external_nat_ip_ids' variable"
  type = bool
  default = false
}

variable "external_nat_ip_ids" {
  description = "List of EIP IDs to be assigned to the NAT Gateways (used in combination with reuse_nat_ips)"
  type = list(string)
  default = []
}

variable "map_public_ip_on_launch" {
  description = "Should be false if you do not want to auto-assign public IP on launch"
  type = bool
  default = true
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type = map(string)
  default = {}
}

variable "vpc_tags" {
  description = "Additional tags for the VPC"
  type = map(string)
  default = {}
}

variable "igw_tags" {
  description = "Additional tags for the internet gateway"
  type = map(string)
  default = {}
}

variable "public_subnet_tags" {
  description = "Additional tags for the public subnets"
  type = map(string)
  default = {}
}

variable "private_subnet_tags" {
  description = "Additional tags for the private subnets"
  type = map(string)
  default = {}
}

variable "public_route_table_tags" {
  description = "Additional tags for the public route tables"
  type = map(string)
  default = {}
}

variable "private_route_table_tags" {
  description = "Additional tags for the private route tables"
  type = map(string)
  default = {}
}

variable "database_route_table_tags" {
  description = "Additional tags for the database route tables"
  type = map(string)
  default = {}
}

variable "database_subnet_tags" {
  description = "Additional tags for the database subnets"
  type = map(string)
  default = {}
}

variable "lambda_subnet_tags" {
  description = "Additional tags for the lambda subnets"
  type = map(string)
  default = {}
}

variable "database_subnet_group_tags" {
  description = "Additional tags for the database subnet group"
  type = map(string)
  default = {}
}

variable "nat_gateway_tags" {
  description = "Additional tags for the NAT gateways"
  type = map(string)
  default = {}
}

variable "nat_eip_tags" {
  description = "Additional tags for the NAT EIP"
  type = map(string)
  default = {}
}

