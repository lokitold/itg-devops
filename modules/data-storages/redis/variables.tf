variable "name" {
  description = "Name for all resources"
  type = string
  default = "redis"
}

variable "region" {
  description = "Region for AWS"
  type = string
}

variable "environment" {
  description = "Environment - Workspace"
  type = string
}

variable "revoke_rules_on_delete" {
  description = "Revoke rules when delete elements"
  type = bool
  default = true
}

variable "enable_single_redis" {
  description = "Enable single Redis"
  type = bool
  default = false
}

variable "enable_cluster" {
  description = "Enable Redis Cluster"
  type = bool
  default = false
}

variable "enable_replicas" {
  description = "Enable Read Replicas in Cluster"
  type = bool
  default = false
}

variable "redis_engine" {
  description = "Redis Engine"
  type = string
  default = "redis"
}

variable "num_cache_nodes" {
  description = "Count of cache nodes for redis"
  type = string
  default = 1
}

variable "num_cache_clusters" {
  description = "Count of cache nodes for redis cluster"
  type = string
  default = 2
}

variable "redis_description" {
  description = "Description for redis"
  type = string
  default = "Redis Cluster"
}

variable "redis_node_type" {
  description = "Node type in Redis"
  type = string
}

variable "redis_port" {
  description = "Port used in Redis"
  type = string
  default = 6379
}

variable "redis_engine_version" {
  description = "Redis Engine Version"
  type = string
  default = "5.0.4"
}

variable "redis_parameter_group_name" {
  description = "Redis Parameter Group Name"
  type = string
  default = "default.redis5.0"
}

variable "cluster_redis_parameter_group_name" {
  description = "Redis Parameter Group Name"
  type = string
  default = "default.redis5.0.cluster.on"
}

variable "redis_automatic_failover_enabled" {
  description = "Redis automatic failover enabled"
  type = bool
  default = true
}

variable "redis_snapshot_retention_limit" {
  description = "Redis snapshot retention limit"
  type = string
  default = 5
}

variable "redis_snapshot_window" {
  description = "Redis snapshot window"
  type = string
  default = "06:00-08:00"
}

variable "redis_maintenance_window" {
  description = "Redis maintenance window"
  type = string
  default = "sun:08:00-sun:09:00"
}

variable "redis_apply_immediately" {
  description = "Redis apply immediately when change any feature"
  type = bool
  default = true
}

variable "redis_cluster_num_node_groups" {
  description = "Number of shards in cluster"
  type = string
  default = 1
}

variable "redis_cluster_replicas_per_node_group" {
  description = "Number of nodes replicas in each shard cluster"
  type = string
  default = 1
}

variable "bucket_name_terraform_backend" {
  description = "Bucket Name used in VPC backend for Terraform"
  type = string
}

variable "vpc_key_path_terraform_backend" {
  description = "Key Path used in VPC backend for Terraform"
  type = string
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type = map(string)
  default = {}
}
