output "redis_endpoint" {
  description = "Redis Endpoint"
  value = coalesce(join("",aws_elasticache_cluster.this.*.cache_nodes.0.address),join("",aws_elasticache_replication_group.this.*.primary_endpoint_address),join("",aws_elasticache_replication_group.cluster.*.configuration_endpoint_address))
}

