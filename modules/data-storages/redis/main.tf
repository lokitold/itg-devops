data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = var.bucket_name_terraform_backend
    key = var.vpc_key_path_terraform_backend
    region = var.region
  }
}

##########
# SG Rules
##########
resource "aws_security_group" "allow-redis" {

  name_prefix = "${var.environment}-allow-redis-"
  description = "Allow Redis traffic"
  vpc_id = data.terraform_remote_state.vpc.outputs.vpc_id

  ingress {
    from_port = 6379
    to_port = 6379
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  revoke_rules_on_delete = var.revoke_rules_on_delete
  lifecycle {
    create_before_destroy = true
  }
  tags = merge(
    var.tags,
    {
      "Name" = format("%s-%s", var.name, var.environment)
    },
  )
}

##############
# Single Redis
##############
resource "aws_elasticache_cluster" "this" {
  count = var.enable_single_redis && (false == var.enable_cluster) ? 1 : 0

  cluster_id = format("%s-%s", var.name, var.environment)
  engine = var.redis_engine
  node_type = var.redis_node_type
  port = var.redis_port
  engine_version = var.redis_engine_version
  num_cache_nodes = var.num_cache_nodes
  parameter_group_name = var.redis_parameter_group_name
  subnet_group_name = data.terraform_remote_state.vpc.outputs.elasticache_subnet_group 
  security_group_ids = ["${aws_security_group.allow-redis.id}"]
  snapshot_retention_limit = var.redis_snapshot_retention_limit # use when node type isn't t2.x or t1.x
  snapshot_window = var.redis_snapshot_window
  maintenance_window = var.redis_maintenance_window 
  apply_immediately = var.redis_apply_immediately

  tags = merge(
    var.tags,
    {
      "Name" = format("%s-%s", var.name, var.environment)
    },
  )
}

###############
# Cluster Redis
###############
resource "aws_elasticache_replication_group" "this" {
  count = var.enable_cluster && (false == var.enable_single_redis) && (false == var.enable_replicas)? 1 : 0

  replication_group_id = format("%s-%s", var.name, var.environment)
  replication_group_description = "${var.redis_description} ${var.environment}"
  node_type = var.redis_node_type
  port = var.redis_port
  engine_version = var.redis_engine_version
  number_cache_clusters = var.num_cache_clusters
  parameter_group_name = var.redis_parameter_group_name
  subnet_group_name = data.terraform_remote_state.vpc.outputs.elasticache_subnet_group 
  security_group_ids = ["${aws_security_group.allow-redis.id}"]
  automatic_failover_enabled = var.redis_automatic_failover_enabled
  snapshot_retention_limit = var.redis_snapshot_retention_limit # use when node type isn't t2.x or t1.x
  snapshot_window = var.redis_snapshot_window
  maintenance_window = var.redis_maintenance_window 
  apply_immediately = var.redis_apply_immediately

  tags = merge(
    var.tags,
    {
      "Name" = format("%s-%s", var.name, var.environment)
    },
  )
}

######################
# Cluster Mode Enabled
######################
resource "aws_elasticache_replication_group" "cluster" {
  count = var.enable_cluster && var.enable_replicas ? 1 : 0

  replication_group_id = format("%s-%s-shards", var.name, var.environment)
  replication_group_description = "${var.redis_description} ${var.environment}"
  node_type = var.redis_node_type
  port = var.redis_port
  engine_version = var.redis_engine_version
  parameter_group_name = var.cluster_redis_parameter_group_name
  subnet_group_name = data.terraform_remote_state.vpc.outputs.elasticache_subnet_group 
  #subnet_group_name = join("", aws_elasticache_subnet_group.this.*.name)
  security_group_ids = ["${aws_security_group.allow-redis.id}"]
  automatic_failover_enabled = var.redis_automatic_failover_enabled
  snapshot_retention_limit = var.redis_snapshot_retention_limit # use when node type isn't t2.x or t1.x
  snapshot_window = var.redis_snapshot_window
  maintenance_window = var.redis_maintenance_window 
  apply_immediately = var.redis_apply_immediately

  cluster_mode {
    num_node_groups = var.redis_cluster_num_node_groups
    replicas_per_node_group = var.redis_cluster_replicas_per_node_group
  }

  tags = merge(
    var.tags,
    {
      "Name" = format("%s-%s", var.name, var.environment)
    },
  )
}

