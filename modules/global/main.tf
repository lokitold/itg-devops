######################################################################
# CREATE AN S3 BUCKET AND DYNAMODB TABLE TO USE AS A TERRAFORM BACKEND
######################################################################
variable "region" {}
variable "name" {}
variable "environment" {}
variable "bucket_name_terraform_state" {}
variable "dynamodb_table_terraform_locks" {}
terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region = var.region
}
######################
# CREATE THE S3 BUCKET
######################
resource "aws_s3_bucket" "terraform_state" {
  bucket = format("%s-%s-%s", var.bucket_name_terraform_state, var.environment, var.region) 

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  lifecycle {
    prevent_destroy = true
  }
}

###########################
# CREATE THE DYNAMODB TABLE
###########################
resource "aws_dynamodb_table" "terraform_locks" {
  name = format("%s-%s-%s", var.dynamodb_table_terraform_locks, var.environment, var.region)
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  lifecycle {
    prevent_destroy = true
  }
}

