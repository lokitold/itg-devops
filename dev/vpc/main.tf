variable "region" {}
variable "name" {}
variable "environment" {}
variable "bucket_name_terraform_state" {}
variable "dynamodb_table_terraform_locks" {}
locals { 
  name = var.name
  region = var.region
  environment = var.environment
  bucket_tfstate = format("%s-%s-%s", var.bucket_name_terraform_state, var.environment, var.region)
  dynamodb_tfstate = format("%s-%s-%s", var.dynamodb_table_terraform_locks, var.environment, var.region)
}
provider "aws" { region = local.region }
terraform {
  required_version = ">= 0.12"
  backend "s3" {
    encrypt = true
  }
}

# EIPS for natgateways
variable "quantity_natgws" {default=2}
resource "aws_eip" "nat" {
  count = var.quantity_natgws
  vpc = true
  
  tags = {
    Name = format("eip-%s-%s-%s", local.name, local.environment, count.index)
    Owner = local.name
    Environment = local.environment
  }
}

module "vpc" {
  source = "../../modules/vpc"

  name = local.name
  environment = local.environment
  azs = ["us-west-1b", "us-west-1c"]
  cidr = "10.30.0.0/16"
  public_subnets = ["10.30.0.0/24","10.30.1.0/24"]
  private_subnets = ["10.30.2.0/24","10.30.3.0/24"]
  database_subnets = ["10.30.4.0/24","10.30.5.0/24"]
  lambda_subnets = ["10.30.6.0/24","10.30.7.0/24"]
  enable_nat_gateway = true
  single_nat_gateway  = true #When only need a natgateway
  #one_nat_gateway_per_az = true
  reuse_nat_ips = true
  external_nat_ip_ids = "${aws_eip.nat.*.id}"

  tags = {
    Owner = local.name
    Environment = local.environment
  }

  vpc_tags = {
    Name = format("vpc-%s-%s",  local.name, local.environment)
  }
}

output "vpc_id" {value=module.vpc.vpc_id}
output "vpc_cidr_block" {value=module.vpc.vpc_cidr_block}
output "public_subnets" {value=module.vpc.public_subnets}
output "private_subnets" {value=module.vpc.private_subnets}
output "lambda_subnets" {value=module.vpc.private_subnets}
output "azs" {value=module.vpc.azs}
output "database_subnets" {value=module.vpc.database_subnets}
output "database_subnet_group" {value=module.vpc.database_subnet_group}
output "elasticache_subnet_group" {value=module.vpc.elasticache_subnet_group}