#!/bin/bash
set -eu

ORIGIN_PATH=${PWD}

GLOBAL_VARS="${ORIGIN_PATH}/global/terraform.tfvars"
 
function exporting_variables(){
    export $(sed -e 's/\(.*\)=/\U\1=/' ${GLOBAL_VARS} | xargs)
}

function global(){
    terraform init
    terraform apply -auto-approve -var-file=${GLOBAL_VARS}
}

function initialize(){
    echo -e "\e[1;34mInicializando recursos en el modulo $CONF_ENV_MODULE..\n$PWD\e[0m"
    if [[ $CONF_ENV_MODULE == "global" ]]; then
        terraform init
    else
        MODULE="$(basename ${PWD})"
        ln -sf ${GLOBAL_VARS} terraform.tfvars
        terraform init \
        -backend-config="bucket=${BUCKET_NAME_TERRAFORM_STATE}-${ENVIRONMENT}-${REGION}" \
        -backend-config="dynamodb_table=${DYNAMODB_TABLE_TERRAFORM_LOCKS}-${ENVIRONMENT}-${REGION}" \
        -backend-config="key=${ENVIRONMENT}/${MODULE}/terraform.tfstate" \
        -backend-config="region=${REGION}"
    fi
}
    
function plane(){
    echo -e "\e[1;34mPlaneando el modulo $CONF_ENV_MODULE..\n$PWD\e[0m"
    terraform plan
}

function provision(){
    echo -e "\e[1;34mProvisionando el modulo $CONF_ENV_MODULE..\n$PWD\e[0m"
    terraform apply -auto-approve
    echo -e "\e[1;34mDespliegue finalizado.. \n\e[0m"
}

function deploy(){
    CONF_ENV_MODULE=$1
    cd ${ORIGIN_PATH}/$CONF_ENV_MODULE
    initialize
    plane
    provision
}

show_menus() {
    echo "Escoger las opciones a realizar para desplegar el ambiente"
    echo "1. global"
    echo "2. vpc"
    echo "3. elastisearch"
    echo "*. Salir"
}
read_options(){
    local choice
    read -p "Escoger: " choice
    case $choice in
    1) deploy global ;;
    2) deploy vpc ;;
    5) deploy data-storages/elasticsearch ;;
    *) exit 0 ;;
    esac
    read -p "Deseas Continuar? (y/n): " RESP
    if [ "$RESP" != "y" ]; then
        exit 0;
    fi
    echo -e "\e[1;34m==========================================================\n\e[0m"
}

trap '' SIGINT SIGQUIT SIGTSTP

while true
do
    exporting_variables
    show_menus
    read_options
    # execution
done