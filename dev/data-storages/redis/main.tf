variable "region" {}
variable "name" {}
variable "environment" {}
variable "bucket_name_terraform_state" {}
variable "dynamodb_table_terraform_locks" {}
locals { 
  name = var.name
  region = var.region
  environment = var.environment
  bucket_tfstate = format("%s-%s-%s", var.bucket_name_terraform_state, var.environment, var.region)
  dynamodb_tfstate = format("%s-%s-%s", var.dynamodb_table_terraform_locks, var.environment, var.region)
  vpc_path_tfstate = "${var.environment}/vpc/terraform.tfstate"
}
provider "aws" { region = local.region }
terraform {
  required_version = ">= 0.12"
  backend "s3" {
    encrypt = true
  }
}

module "redis" {

  source = "../../../modules/data-storages/redis"

  bucket_name_terraform_backend = local.bucket_tfstate
  vpc_key_path_terraform_backend = local.vpc_path_tfstate
  region = local.region
  environment = local.environment
  redis_node_type = "cache.t2.medium"
  
  #enable_single_redis = true #When use single
  enable_cluster = true #When use cluster
  #enable_replicas = true #Only when cluster is true

  tags = {
    Name = format("redis-%s", local.environment)
    Owner = local.name
    Environment = local.environment
  }
}

output "redis_endpoint" {value=module.redis.redis_endpoint}